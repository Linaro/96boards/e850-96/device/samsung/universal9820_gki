/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "audio_hw_proxy_usb"
#define LOG_NDEBUG 0

//#define VERY_VERY_VERBOSE_LOGGING
#ifdef VERY_VERY_VERBOSE_LOGGING
#define ALOGVV ALOGD
#else
#define ALOGVV(a...) do { } while(0)
#endif

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/stat.h>
#include <dlfcn.h>
#include <unistd.h>

#include <log/log.h>
#include <cutils/str_parms.h>

#include <audio_utils/channels.h>
#include <audio_utils/primitives.h>
#include <audio_utils/clock.h>
#include <tinyalsa/asoundlib.h>

#include <system/audio.h>
#include <hardware/hardware.h>
#include <hardware/audio.h>
#include <hardware/audio_alsaops.h>

#include <audio_utils/resampler.h>

#include "alsa_device_profile.h"
#include "alsa_device_proxy.h"
#include "alsa_logging.h"

#include "audio_usb_proxy.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

/* format in order of increasing preference */
static const int pcm_format_preference_map[] = {
    PCM_FORMAT_S8,
    PCM_FORMAT_S16_LE,
    PCM_FORMAT_S24_LE,
    PCM_FORMAT_S24_3LE,
    PCM_FORMAT_S32_LE
};

/******************************************************************************/
/**                                                                          **/
/** Audio Proxy is Singleton                                                 **/
/**                                                                          **/
/******************************************************************************/

static struct audio_proxy_usb *usb_instance = NULL;

static struct audio_proxy_usb* getUSBInstance(void)
{
    if (usb_instance == NULL) {
        usb_instance = calloc(1, sizeof(struct audio_proxy_usb));
        ALOGI("proxy-%s: created Audio Proxy USB Instance!", __func__);
    }
    return usb_instance;
}

static void destroyUSBInstance(void)
{
    if (usb_instance) {
        free(usb_instance);
        usb_instance = NULL;
        ALOGI("proxy-%s: destroyed Audio Proxy USB Instance!", __func__);
    }
    return;
}

/******************************************************************************/
/**                                                                          **/
/** Local Functions of USB Audio Proxy                                       **/
/**                                                                          **/
/******************************************************************************/
/* Functions should be called with usb_lock mutex */

// This function is to load usb gain mixer paths xml file
static int usb_audio_gain_load_xml(void *proxy, int usb_card)
{
    struct audio_proxy_usb *aproxy_usb = proxy;
    char gain_mixer_path[MAX_USB_PATH_LEN];
    int ret = 0;

    memset(gain_mixer_path, 0, MAX_USB_PATH_LEN);

    // read gain xml based on PID values
    if (aproxy_usb->usb_pid == USB_BUNDLE_WHITE_PID) {
        strcpy(gain_mixer_path, USB_BUNDLE_WHITE_GAIN_XML_MIXER_PATH);
        ALOGI("proxy-%s: USB White Bundle GainControl XML [%s] loading",
            __func__, gain_mixer_path);
    } else {
        strcpy(gain_mixer_path, USB_BUNDLE_GRAY_GAIN_XML_MIXER_PATH);
        ALOGI("proxy-%s: USB Gray Bundle GainControl XML [%s] loading",
            __func__, gain_mixer_path);
    }

    //initialize audio_route with gain xml
    aproxy_usb->usb_ar = audio_route_init(usb_card, gain_mixer_path);
    if (!aproxy_usb->usb_ar) {
        ALOGE("proxy-%s: failed to init audio route for USB Gain usb_card: %d",
            __func__, usb_card);
        ret = -EINVAL;
    }

    return ret;
}

// This function is to load usb gain mixer paths xml file
static void usb_audio_gain_unload_xml(void *proxy)
{
    struct audio_proxy_usb *aproxy_usb = proxy;

    if (aproxy_usb->usb_ar) {
        audio_route_free(aproxy_usb->usb_ar);
        aproxy_usb->usb_ar = NULL;
    }

    return;
}

/* Check to VID (Vendor ID):PID (Product ID) of USB Device and enable gain-control */
static void usb_audio_gain_control_enable(void *proxy)
{
    struct audio_proxy_usb *aproxy_usb = proxy;
    char path[MAX_USB_PATH_LEN];
    char readbuf[USB_READ_SIZE];
    char *endptr;
    int usb_card = -1;
    int fd = -1;
    int ret = 0;

    if (!aproxy_usb->usb_gaincontrol_needed &&
        (aproxy_usb->usb_out_connected || aproxy_usb->usb_in_connected)) {
        //get valid usb card number
        if (aproxy_usb->usb_out_pcm_card != -1 ||
            aproxy_usb->usb_in_pcm_card != -1) {
            usb_card = ((aproxy_usb->usb_out_pcm_card != -1) ?
                        aproxy_usb->usb_out_pcm_card :
                        aproxy_usb->usb_in_pcm_card);
        } else {
            ALOGE("%s: failed get valid usb card", __func__);
            goto err;
        }

        // get VID:PID information from usb device node
        memset(path, 0, sizeof(path));
        ret = snprintf(path, sizeof(path), "/proc/asound/card%u/usbid",
                     usb_card);
        if (ret < 0) {
            ALOGE("%s: snprintf failed ret (%d)", __func__, ret);
            goto err;
        }

        fd = open(path, O_RDONLY);
        if (fd < 0) {
            ALOGE("%s: failed to open usbid file %s error: %d",
                  __func__, path, errno);
            goto err;
        }

        if(read(fd, readbuf, USB_READ_SIZE) < 0) {
           ALOGE("file read error");
           goto err;
       }

        //extract VID and PID from string separated by colon
        aproxy_usb->usb_vid = (int)strtol(readbuf, &endptr, 16);
        if (endptr == NULL || *endptr == '\0' || *endptr != ':') {
            ALOGE("failed to parse USB VID");
            aproxy_usb->usb_vid = -1;
            goto err;
        }
        aproxy_usb->usb_pid = (int)strtol((endptr+1), &endptr, 16);

        ALOGI("proxy-%s: USB Device VID: 0x%x PID: 0x%x", __func__,
            aproxy_usb->usb_vid, aproxy_usb->usb_pid);
        // check VID & PID, for gain-control
        if (aproxy_usb->usb_vid == USB_BUNDLE_VID &&
            (aproxy_usb->usb_pid == USB_BUNDLE_WHITE_PID ||
            aproxy_usb->usb_pid == USB_BUNDLE_GRAY_HEADPHONE_PID ||
            aproxy_usb->usb_pid == USB_BUNDLE_GRAY_HEADSET_PID)) {
                if (!usb_audio_gain_load_xml(aproxy_usb, usb_card)) {
                    aproxy_usb->usb_gaincontrol_needed = true;
                    ALOGI("proxy-%s: USB GainControl enabled", __func__);
                } else {
                    ALOGW("proxy-%s: failed to load USB gain XML", __func__);
                }
        } else {
            ALOGI("proxy-%s: USB GainControl not required", __func__);
        }
    } else {
        if (aproxy_usb->usb_gaincontrol_needed)
            ALOGI("proxy-%s: USB GainControl already enabled", __func__);
        else
            ALOGI("proxy-%s: USB Device not connected", __func__);
    }

err:
    if (fd >= 0) close(fd);
    return;
}

static void usb_audio_gain_control_disable(void *proxy)
{
    struct audio_proxy_usb *aproxy_usb = proxy;

    if (aproxy_usb->usb_gaincontrol_needed &&
        (!aproxy_usb->usb_out_connected && !aproxy_usb->usb_in_connected)) {
            usb_audio_gain_unload_xml(aproxy_usb);
            aproxy_usb->usb_gaincontrol_needed = false;
            ALOGI("proxy-%s: USB GainControl disabled", __func__);
    } else if (aproxy_usb->usb_gaincontrol_needed) {
        ALOGI("proxy-%s: USB Device still in use", __func__);
    }

    return;
}

// To pick the best audio profile for connected usb device
static void usb_config_pick_audio_profile(
    alsa_device_profile *usb_profile,
    struct pcm_config *proxy_config)
{
    int best_format_index = -1;
    int index = 0;
    int i = 0;

    if (profile_is_valid(usb_profile)) {
        for (i = 0; usb_profile->formats[i] != PCM_FORMAT_INVALID; i++) {
            for (index = 0; index < (int)ARRAY_SIZE(pcm_format_preference_map); index ++) {
                if ((usb_profile->formats[i] == pcm_format_preference_map[index])
                    && (index > best_format_index)) {
                    best_format_index = index;
                    break;
                }
            }
        }
        if (best_format_index > -1) {
            proxy_config->format = pcm_format_preference_map[best_format_index];
            ALOGI("proxy-%s: pick output format (%d)", __func__, proxy_config->format);
        }

        // In case of Sampling Rates and Channel Counts, default config is the best one so we keep the value now.
        // If the Sampling Rates and Channel Counts need to be changed, please add the code here.
    }
}

static void usb_config_out_proxy(void *proxy)
{
    struct audio_proxy_usb *aproxy_usb = proxy;
    alsa_device_profile *usb_out_profile = &aproxy_usb->usb_out_profile;
    struct pcm_config proxy_config;

    if (aproxy_usb->usb_out_connected) {
        if (usb_out_profile) {
            memset(&proxy_config, 0, sizeof(proxy_config));

            usb_out_profile->card = aproxy_usb->usb_out_pcm_card;
            usb_out_profile->device = aproxy_usb->usb_out_pcm_device;

            // Sets PCM Configuration
            profile_read_device_info(usb_out_profile);

            // Check Supported Sampling Rates
            char * rates_list = profile_get_sample_rate_strs(usb_out_profile);
            ALOGI("proxy-%s: Out SR List = %s", __func__, rates_list);
            free(rates_list);

            proxy_config.rate = profile_get_default_sample_rate(usb_out_profile);
            ALOGI("proxy-%s: default output sampling rate (%d)", __func__, proxy_config.rate);

            // Check Supported Sample Formats
            char * format_params = profile_get_format_strs(usb_out_profile);
            ALOGI("proxy-%s: Out FR List = %s", __func__, format_params);
            free(format_params);

            proxy_config.format = profile_get_default_format(usb_out_profile);
            ALOGI("proxy-%s: default output format (%d)", __func__, proxy_config.format);

            // Check Supported Channel Counts
            char* channels_list = profile_get_channel_count_strs(usb_out_profile);
            ALOGI("proxy-%s: Out CC List = %s", __func__, channels_list);
            free(channels_list);

            proxy_config.channels = profile_get_default_channel_count(usb_out_profile);
            ALOGI("proxy-%s: default output channel count (%d)", __func__, proxy_config.channels);

            // Pick the best Audio Profile
            usb_config_pick_audio_profile(usb_out_profile, &proxy_config);

            proxy_prepare(&aproxy_usb->usb_out_proxy, usb_out_profile, &proxy_config);
            ALOGI("proxy-%s: configured USB Out Proxy SR(%d) CH(%d) FMT(%d)", __func__,
            proxy_config.rate, proxy_config.channels, proxy_config.format);

            aproxy_usb->usb_out_cpcall_prepared = false;
        }
    }

    return ;
}

static void usb_config_in_proxy(void *proxy)
{
    struct audio_proxy_usb *aproxy_usb = proxy;
    alsa_device_profile *usb_in_profile = &aproxy_usb->usb_in_profile;
    struct pcm_config proxy_config;

    if (aproxy_usb->usb_in_connected) {
        if (usb_in_profile) {
            memset(&proxy_config, 0, sizeof(proxy_config));

            usb_in_profile->card = aproxy_usb->usb_in_pcm_card;
            usb_in_profile->device = aproxy_usb->usb_in_pcm_device;

            // Sets PCM Configuration
            profile_read_device_info(usb_in_profile);

            // Check Supported Sampling Rates
            char * rates_list = profile_get_sample_rate_strs(usb_in_profile);
            ALOGI("proxy-%s: In SR List = %s", __func__, rates_list);
            free(rates_list);

            if (profile_is_sample_rate_valid(usb_in_profile, DEFAULT_USB_MEDIA_SAMPLING_RATE))
                proxy_config.rate = DEFAULT_USB_MEDIA_SAMPLING_RATE;
            else {
                ALOGE("proxy-%s: invalid input sampling rate (%d)", __func__, DEFAULT_USB_MEDIA_SAMPLING_RATE);
                proxy_config.rate = profile_get_default_sample_rate(usb_in_profile);
                ALOGI("proxy-%s: default input sampling rate (%d)", __func__, proxy_config.rate);
            }

            // Check Supported Sample Formats
            char * format_params = profile_get_format_strs(usb_in_profile);
            ALOGI("proxy-%s: In FR List = %s", __func__, format_params);
            free(format_params);

            if (profile_is_format_valid(usb_in_profile, DEFAULT_USB_MEDIA_FORMAT))
                proxy_config.format = DEFAULT_USB_MEDIA_FORMAT;
            else {
                ALOGE("proxy-%s: invalid input format (%d)", __func__, DEFAULT_USB_MEDIA_FORMAT);
                proxy_config.format = profile_get_default_format(usb_in_profile);
                ALOGI("proxy-%s: default input format (%d)", __func__, proxy_config.format);
            }

            // Check Supported Channel Counts
            char* channels_list = profile_get_channel_count_strs(usb_in_profile);
            ALOGI("proxy-%s: In CC List = %s", __func__, channels_list);
            free(channels_list);

            if (profile_is_channel_count_valid(usb_in_profile, DEFAULT_USB_MEDIA_CHANNELS))
                proxy_config.channels = DEFAULT_USB_MEDIA_CHANNELS;
            else {
                ALOGE("proxy-%s: invalid input channel count (%d)", __func__, DEFAULT_USB_MEDIA_CHANNELS);
                proxy_config.channels = profile_get_default_channel_count(usb_in_profile);
                ALOGI("proxy-%s: default input channel count (%d)", __func__, proxy_config.channels);
            }

            proxy_prepare(&aproxy_usb->usb_in_proxy, usb_in_profile, &proxy_config);
            ALOGI("proxy-%s: configured USB In Proxy SR(%d) CH(%d) FMT(%d)", __func__,
                        proxy_config.rate, proxy_config.channels, proxy_config.format);
        }
    }

    return ;
}

/* Function should be called with usb_lock mutex */
static void usb_open_out_proxy(struct audio_proxy_usb *aproxy_usb)
{
    char pcm_path[MAX_USB_PATH_LEN];

    if (aproxy_usb && aproxy_usb->usb_out_connected) {
        if (aproxy_usb->usb_out_status == false) {
            if (proxy_open(&aproxy_usb->usb_out_proxy) == 0) {

                // Dummy write to trigger pcm_prepare
                unsigned int size = proxy_get_period_size(&aproxy_usb->usb_out_proxy);
                uint16_t *dummy = (uint16_t *)calloc(1, size);
                if (dummy && proxy_write(&aproxy_usb->usb_out_proxy, (void *)dummy, size) == 0) {
                    snprintf(pcm_path, sizeof(pcm_path), "/dev/snd/pcmC%uD%u%c",
                             aproxy_usb->usb_out_profile.card, aproxy_usb->usb_out_profile.device, 'p');


                    ALOGI("%s-%s: The opened USB Out PCM Device is %s with SR(%d), CC(%d), Format(%d)",
                        "usb-out", __func__, pcm_path,
                        proxy_get_sample_rate(&aproxy_usb->usb_out_proxy),
                        proxy_get_channel_count(&aproxy_usb->usb_out_proxy),
                        proxy_get_format(&aproxy_usb->usb_out_proxy));

                    aproxy_usb->usb_out_status = true;
                } else {
                    ALOGE("%s-%s: USB Out PCM Device cannot be started",
                            "usb-out", __func__);
                    proxy_close(&aproxy_usb->usb_out_proxy);
                }

                if (dummy)
                    free(dummy);
            } else
                ALOGE("%s-%s: USB Out PCM Device cannot be opened",
                        "usb-out", __func__);
        }
    }

    return ;
}

/* Function should be called with usb_lock mutex */
static void usb_close_out_proxy(struct audio_proxy_usb *aproxy_usb)
{
    if (aproxy_usb && aproxy_usb->usb_out_connected) {
        if (aproxy_usb->usb_out_status == true) {
            proxy_close(&aproxy_usb->usb_out_proxy);
            ALOGI("proxy-%s: closed USB Out PCM Device", __func__);

            aproxy_usb->usb_out_status = false;
        }
    }

    return ;
}

static void usb_open_in_proxy(struct audio_proxy_usb *aproxy_usb)
{
    char pcm_path[MAX_USB_PATH_LEN];

    if (aproxy_usb && aproxy_usb->usb_in_connected
        /*&& aproxy_usb->active_capture_device == DEVICE_USB_HEADSET_MIC*/) {
        if (proxy_open(&aproxy_usb->usb_in_proxy) == 0) {
            if (proxy_start(&aproxy_usb->usb_in_proxy) == 0) {
                snprintf(pcm_path, sizeof(pcm_path), "/dev/snd/pcmC%uD%u%c",
                         aproxy_usb->usb_in_profile.card, aproxy_usb->usb_in_profile.device, 'c');

                ALOGI("%s-%s: The opened USB In PCM Device is %s with SR(%d), CC(%d), Format(%d)",
                      "usb-in", __func__, pcm_path,
                      proxy_get_sample_rate(&aproxy_usb->usb_in_proxy),
                      proxy_get_channel_count(&aproxy_usb->usb_in_proxy),
                      proxy_get_format(&aproxy_usb->usb_in_proxy));
            } else {
                ALOGE("%s-%s: The USB In PCM Device cannot be started",
                      "usb-in", __func__);
                proxy_close(&aproxy_usb->usb_in_proxy);
            }
        } else
            ALOGE("%s-%s: The USB In PCM Device cannot be opened",
                  "usb-in", __func__);
    }

    return ;
}

static void usb_close_in_proxy(struct audio_proxy_usb *aproxy_usb)
{
    if (aproxy_usb && aproxy_usb->usb_in_connected) {
        proxy_close(&aproxy_usb->usb_in_proxy);
        ALOGI("proxy-%s: closed USB In PCM Device", __func__);
    }

    return ;
}

static bool parse_card_device_params(const char *kvpairs, int *card, int *device)
{
    struct str_parms * parms = str_parms_create_str(kvpairs);
    char value[32];
    int param_val;

    // initialize to "undefined" state.
    *card = -1;
    *device = -1;

    param_val = str_parms_get_str(parms, "card", value, sizeof(value));
    if (param_val >= 0) {
        *card = atoi(value);
    }

    param_val = str_parms_get_str(parms, "device", value, sizeof(value));
    if (param_val >= 0) {
        *device = atoi(value);
    }

    str_parms_destroy(parms);

    return *card >= 0 && *device >= 0;
}

/******************************************************************************/
/**                                                                          **/
/** Interface Functions of USB Audio Proxy                                   **/
/**                                                                          **/
/******************************************************************************/
int proxy_is_usb_playback_CPCall_prepared(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    return aproxy_usb->usb_out_cpcall_prepared;
}

int proxy_is_usb_playback_device_connected(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    return aproxy_usb->usb_out_connected;
}

int proxy_is_usb_capture_device_connected(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    return aproxy_usb->usb_in_connected;
}

unsigned int proxy_usb_get_capture_samplerate(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    alsa_device_proxy   *usb_proxy = &aproxy_usb->usb_in_proxy;

    return proxy_get_sample_rate(usb_proxy);
    //return aproxy_usb->usb_in_rate;
}

unsigned int proxy_usb_get_capture_channels(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    alsa_device_proxy   *usb_proxy = &aproxy_usb->usb_in_proxy;

    return proxy_get_channel_count(usb_proxy);
    //return aproxy_usb->usb_in_channels;
}

int proxy_usb_get_capture_format(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    alsa_device_proxy   *usb_proxy = &aproxy_usb->usb_in_proxy;

    return proxy_get_format(usb_proxy);
}

int proxy_usb_get_playback_samplerate(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    alsa_device_proxy   *usb_proxy = &aproxy_usb->usb_out_proxy;

    return proxy_get_sample_rate(usb_proxy);
}

int proxy_usb_get_playback_channels(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    alsa_device_proxy   *usb_proxy = &aproxy_usb->usb_out_proxy;

    return proxy_get_channel_count(usb_proxy);
}

int proxy_usb_get_playback_format(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    alsa_device_proxy   *usb_proxy = &aproxy_usb->usb_out_proxy;

    return proxy_get_format(usb_proxy);
}

int proxy_usb_get_playback_bitwidth(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    alsa_device_proxy   *usb_proxy = &aproxy_usb->usb_out_proxy;
    int ret = 0;

    switch (proxy_get_format(usb_proxy)) {
    case PCM_FORMAT_S16_LE:  /* 16-bit signed */
        ret = 16;
        break;
    case PCM_FORMAT_S32_LE:      /* 32-bit signed */
        ret = 32;
        break;
    case PCM_FORMAT_S24_LE:      /* 24-bits in 4-bytes */
    case PCM_FORMAT_S24_3LE:     /* 24-bits in 3-bytes */
        ret = 24;
        break;
    case PCM_FORMAT_S8:          /* 8-bit signed */
    default:
        ret = 16;
        break;
    }

    return ret;
}

void proxy_usb_playback_prepare(void *proxy_usb, bool set_default)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    struct pcm_config proxy_config;
    unsigned int rate = 0, channels = 0;
    enum pcm_format format;
    bool update_config = false;
    alsa_device_profile *usb_profile = &aproxy_usb->usb_out_profile;
    alsa_device_proxy   *usb_proxy = &aproxy_usb->usb_out_proxy;

    // Get current USB Output PCM Configuration
    proxy_config.rate = proxy_get_sample_rate(usb_proxy);
    proxy_config.format = proxy_get_format(usb_proxy);
    proxy_config.channels = proxy_get_channel_count(usb_proxy);
    proxy_config.period_size = proxy_get_period_size(usb_proxy);
    proxy_config.period_count = proxy_get_period_count(usb_proxy);

    // Configure sample rate based on flag
    // set_default: 'false' means CPCall configuration 48KHz, 16bit, 2CH or supported
    // set_default: 'true' means USB maximum possible configuration
    if (set_default) {
        // Get default Sample rate
        rate = profile_get_default_sample_rate(usb_profile);
        if (rate != proxy_config.rate) {
            proxy_config.rate = rate;
            update_config = true;
        }
        ALOGI("proxy-%s: default output sampling rate (%d)", __func__, proxy_config.rate);

        // Get default Sample Format
        format = profile_get_default_format(usb_profile);
        if (format != proxy_config.format) {
            proxy_config.format = format;
            update_config = true;
        }
        ALOGI("proxy-%s: default output format (%d)", __func__, proxy_config.format);

        // Get default Channel Counts
        channels  = profile_get_default_channel_count(usb_profile);
        if (channels != proxy_config.channels) {
            proxy_config.channels = channels;
            update_config = true;
        }
        ALOGI("proxy-%s: default output channel count (%d)", __func__, proxy_config.channels);

        // Pick the best Audio Profile
        usb_config_pick_audio_profile(usb_profile, &proxy_config);
        if ((format != proxy_config.format)
            || (channels != proxy_config.channels)
            || (rate != proxy_config.rate)) {
            format = proxy_config.format;
            channels = proxy_config.channels;
            rate = proxy_config.rate;
            update_config = true;
            ALOGI("proxy-%s: best output profile picked rate (%d) format (%d) channel count (%d)",
                __func__, proxy_config.rate, proxy_config.format, proxy_config.channels);
        }

        aproxy_usb->usb_out_cpcall_prepared = false;
    } else { // CPCall fixed or supported configuration
        // Check fixed sample rate is valid or not
        if (proxy_config.rate != DEFAULT_USB_MEDIA_SAMPLING_RATE) {
            if (profile_is_sample_rate_valid(usb_profile, DEFAULT_USB_MEDIA_SAMPLING_RATE)) {
                proxy_config.rate = DEFAULT_USB_MEDIA_SAMPLING_RATE;
                ALOGI("proxy-%s: fixed output sampling rate (%d)", __func__, proxy_config.rate);
            } else {
                ALOGE("proxy-%s: invalid output sampling rate (%d)", __func__,
                    DEFAULT_USB_MEDIA_SAMPLING_RATE);
                proxy_config.rate = profile_get_default_sample_rate(usb_profile);
                ALOGI("proxy-%s: default output sampling rate (%d) is configured", __func__,
                    proxy_config.rate);
            }
            update_config = true;
        }

        // Check fixed format is valid or not
        if (proxy_config.format != DEFAULT_USB_MEDIA_FORMAT) {
            if (profile_is_format_valid(usb_profile, DEFAULT_USB_MEDIA_FORMAT)) {
                proxy_config.format = DEFAULT_USB_MEDIA_FORMAT;
                ALOGI("proxy-%s: fixed output format (%d)", __func__, proxy_config.format);
            } else {
                ALOGE("proxy-%s: invalid output format (%d)", __func__, DEFAULT_USB_MEDIA_FORMAT);
                proxy_config.format = profile_get_default_format(usb_profile);
                ALOGI("proxy-%s: default output format (%d)", __func__, proxy_config.format);
            }
            update_config = true;
        }

        // Check fixed channel count is valid or not
        if (proxy_config.channels != DEFAULT_USB_MEDIA_CHANNELS) {
            if (profile_is_channel_count_valid(usb_profile, DEFAULT_USB_MEDIA_CHANNELS)) {
                proxy_config.channels = DEFAULT_USB_MEDIA_CHANNELS;
                ALOGI("proxy-%s: fixed output channel count (%d)", __func__, proxy_config.channels);
            } else {
                ALOGE("proxy-%s: invalid output channel count (%d)", __func__, DEFAULT_USB_MEDIA_CHANNELS);
                proxy_config.channels = profile_get_default_channel_count(usb_profile);
                ALOGI("proxy-%s: default output channel count (%d)", __func__, proxy_config.channels);
            }
            update_config = true;
        }

        aproxy_usb->usb_out_cpcall_prepared = true;
    }

    // prepare USB device with required configuration
    if (update_config)
        proxy_prepare(usb_proxy, usb_profile, &proxy_config);
    ALOGI("proxy-%s: configured USB Out Proxy SR(%d) CH(%d) FMT(%d)", __func__,
            proxy_config.rate, proxy_config.channels, proxy_config.format);

    return;
}

int proxy_usb_getparam_playback_stream(void *proxy_usb, void *query_params, void *reply_params)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    struct str_parms *query = (struct str_parms *)query_params;
    struct str_parms *reply = (struct str_parms *)reply_params;

    alsa_device_profile *profile = &aproxy_usb->usb_out_profile;

    if (profile->card >= 0 || profile->device >= 0) {
        // supported sample formats
        if (str_parms_has_key(query, AUDIO_PARAMETER_STREAM_SUP_FORMATS)) {
            char * format_list = profile_get_format_strs(profile);
            if (format_list) {
                str_parms_add_str(reply, AUDIO_PARAMETER_STREAM_SUP_FORMATS, format_list);
                free(format_list);
            }
        }

        // supported channel counts
        if (str_parms_has_key(query, AUDIO_PARAMETER_STREAM_SUP_CHANNELS)) {
            char* channels_list = profile_get_channel_count_strs(profile);
            if (channels_list) {
                str_parms_add_str(reply, AUDIO_PARAMETER_STREAM_SUP_CHANNELS, channels_list);
                free(channels_list);
            }
        }

        // supported sample rates
        if (str_parms_has_key(query, AUDIO_PARAMETER_STREAM_SUP_SAMPLING_RATES)) {
            char* rates_list = profile_get_sample_rate_strs(profile);
            if (rates_list) {
                str_parms_add_str(reply, AUDIO_PARAMETER_STREAM_SUP_SAMPLING_RATES, rates_list);
                free(rates_list);
            }
        }
    }

    return 0;
}

int proxy_usb_setparam_playback_stream(void *proxy_usb, void *parameters)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    int ret = 0;

    alsa_device_profile *profile = &aproxy_usb->usb_out_profile;
    int card = -1;
    int device = -1;

    if (!parse_card_device_params((const char *)parameters, &card, &device)) {
        // nothing to do
        return ret;
    }

    if (card >= 0 && device >= 0) {
        int saved_card = profile->card;
        int saved_device = profile->device;

        if (saved_card != card || saved_device != device) {
            profile->card = card;
            profile->device = device;
            ret = profile_read_device_info(profile) ? 0 : -EINVAL;
            if (ret != 0) {
                profile->card = saved_card;
                profile->device = saved_device;
                ALOGI("%s-%s: updated USB Card %d Device %d", "usb-out",
                      __func__, profile->card, profile->device);
            }
        } else
            ALOGV("%s-%s: requested same USB Card %d Device %d", "usb-out",
                  __func__, profile->card, profile->device);
    }

    return ret;
}

void proxy_usb_capture_prepare(void *proxy_usb, bool set_default)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    struct pcm_config proxy_config;
    unsigned int rate = 0, channels = 0;
    enum pcm_format format;
    bool update_config = false;
    alsa_device_profile *usb_profile = &aproxy_usb->usb_in_profile;
    alsa_device_proxy   *usb_proxy = &aproxy_usb->usb_in_proxy;

    // Get current USB Output PCM Configuration
    proxy_config.rate = proxy_get_sample_rate(usb_proxy);
    proxy_config.format = proxy_get_format(usb_proxy);
    proxy_config.channels = proxy_get_channel_count(usb_proxy);
    proxy_config.period_size = proxy_get_period_size(usb_proxy);
    proxy_config.period_count = proxy_get_period_count(usb_proxy);

    // Configure sample rate based on flag
    // set_default: 'false' means tol configuration 48KHz, 16bit, 2CH or supported
    // set_default: 'true' means USB maximum possible configuration
    if (set_default) {
        // Get default Sample rate
        rate = profile_get_default_sample_rate(usb_profile);
        if (rate != proxy_config.rate) {
            proxy_config.rate = rate;
            update_config = true;
        }
        ALOGI("proxy-%s: default input sampling rate (%d)", __func__, proxy_config.rate);

        // Get default Sample Format
        format = profile_get_default_format(usb_profile);
        if (format != proxy_config.format) {
            proxy_config.format = format;
            update_config = true;
        }
        ALOGI("proxy-%s: default input format (%d)", __func__, proxy_config.format);

        // Get default Channel Counts
        channels  = profile_get_default_channel_count(usb_profile);
        if (channels != proxy_config.channels) {
            proxy_config.channels = channels;
            update_config = true;
        }
        ALOGI("proxy-%s: default input channel count (%d)", __func__, proxy_config.channels);

        aproxy_usb->usb_out_cpcall_prepared = false;
    } else { // fixed or supported configuration
        // Check fixed sample rate is valid or not
        if (proxy_config.rate != DEFAULT_USB_MEDIA_SAMPLING_RATE) {
            if (profile_is_sample_rate_valid(usb_profile, DEFAULT_USB_MEDIA_SAMPLING_RATE)) {
                proxy_config.rate = DEFAULT_USB_MEDIA_SAMPLING_RATE;
                ALOGI("proxy-%s: fixed input sampling rate (%d)", __func__, proxy_config.rate);
            } else {
                ALOGE("proxy-%s: invalid input sampling rate (%d)", __func__,
                    DEFAULT_USB_MEDIA_SAMPLING_RATE);
                proxy_config.rate = profile_get_default_sample_rate(usb_profile);
                ALOGI("proxy-%s: default input sampling rate (%d) is configured", __func__,
                    proxy_config.rate);
            }
            update_config = true;
        }

        // Check fixed format is valid or not
        if (proxy_config.format != DEFAULT_USB_MEDIA_FORMAT) {
            if (profile_is_format_valid(usb_profile, DEFAULT_USB_MEDIA_FORMAT)) {
                proxy_config.format = DEFAULT_USB_MEDIA_FORMAT;
                ALOGI("proxy-%s: fixed input format (%d)", __func__, proxy_config.format);
            } else {
                ALOGE("proxy-%s: invalid input format (%d)", __func__, DEFAULT_USB_MEDIA_FORMAT);
                proxy_config.format = profile_get_default_format(usb_profile);
                ALOGI("proxy-%s: default input format (%d)", __func__, proxy_config.format);
            }
            update_config = true;
        }

        // Check fixed channel count is valid or not
        if (proxy_config.channels != DEFAULT_USB_MEDIA_CHANNELS) {
            if (profile_is_channel_count_valid(usb_profile, DEFAULT_USB_MEDIA_CHANNELS)) {
                proxy_config.channels = DEFAULT_USB_MEDIA_CHANNELS;
                ALOGI("proxy-%s: fixed input channel count (%d)", __func__, proxy_config.channels);
            } else {
                ALOGE("proxy-%s: invalid input channel count (%d)", __func__, DEFAULT_USB_MEDIA_CHANNELS);
                proxy_config.channels = profile_get_default_channel_count(usb_profile);
                ALOGI("proxy-%s: default input channel count (%d)", __func__, proxy_config.channels);
            }
            update_config = true;
        }
    }

    // prepare USB device with required configuration
    if (update_config)
        proxy_prepare(usb_proxy, usb_profile, &proxy_config);

    ALOGI("proxy-%s: configured USB InProxy SR(%d) CH(%d) FMT(%d)", __func__,
            proxy_config.rate, proxy_config.channels, proxy_config.format);

    return;
}

int proxy_usb_getparam_capture_stream(void *proxy_usb, void *query_params, void *reply_params)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    struct str_parms *query = (struct str_parms *)query_params;
    struct str_parms *reply = (struct str_parms *)reply_params;

    alsa_device_profile *profile = &aproxy_usb->usb_in_profile;

    if (profile->card >= 0 && profile->device >= 0) {
        // supported sample formats
        if (str_parms_has_key(query, AUDIO_PARAMETER_STREAM_SUP_FORMATS)) {
            char* format_list = profile_get_format_strs(profile);
            if (format_list) {
                str_parms_add_str(reply, AUDIO_PARAMETER_STREAM_SUP_FORMATS, format_list);
                free(format_list);
            }
        }

        // supported channel counts
        if (str_parms_has_key(query, AUDIO_PARAMETER_STREAM_SUP_CHANNELS)) {
            char* channels_list = profile_get_channel_count_strs(profile);
            if (channels_list) {
                str_parms_add_str(reply, AUDIO_PARAMETER_STREAM_SUP_CHANNELS, channels_list);
                free(channels_list);
            }
        }

        /* supported sample rates */
        if (str_parms_has_key(query, AUDIO_PARAMETER_STREAM_SUP_SAMPLING_RATES)) {
            char* rates_list = profile_get_sample_rate_strs(profile);
            if (rates_list) {
                str_parms_add_str(reply, AUDIO_PARAMETER_STREAM_SUP_SAMPLING_RATES, rates_list);
                free(rates_list);
            }
        }
    }

    return 0;
}

int proxy_usb_setparam_capture_stream(void *proxy_usb, void *parameters)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    alsa_device_profile *profile = &aproxy_usb->usb_in_profile;
    int card = -1;
    int device = -1;
    int ret = 0;

    if (!parse_card_device_params((const char *)parameters, &card, &device)) {
        // nothing to do
        return ret;
    }

    if (card >= 0 && device >= 0) {
        int saved_card = profile->card;
        int saved_device = profile->device;

        if (saved_card != card || saved_device != device) {
            profile->card = card;
            profile->device = device;
            ret = profile_read_device_info(profile) ? 0 : -EINVAL;
            if (ret != 0) {
                profile->card = saved_card;
                profile->device = saved_device;
                ALOGE("%s-%s: failed to read device info", "usb-in", __func__);
            } else {
                ALOGI("%s-%s: USB Capture device initialized for Card %d Device %d",
                      "usb-in", __func__, profile->card, profile->device);

                //prepare_capture_usbproxy(apstream);
            }
        } else
            ALOGV("%s-%s: requested same USB Card %d Device %d", "usb-in",
                  __func__, profile->card, profile->device);
    }

    return ret;
}

void proxy_usb_open_out_proxy(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    pthread_mutex_lock(&aproxy_usb->usb_lock);

    usb_open_out_proxy(aproxy_usb);

    pthread_mutex_unlock(&aproxy_usb->usb_lock);

    return;
}
void proxy_usb_close_out_proxy(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    pthread_mutex_lock(&aproxy_usb->usb_lock);

    usb_close_out_proxy(aproxy_usb);

    pthread_mutex_unlock(&aproxy_usb->usb_lock);

    return;
}
void proxy_usb_open_in_proxy(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    pthread_mutex_lock(&aproxy_usb->usb_lock);

    usb_open_in_proxy(aproxy_usb);

    pthread_mutex_unlock(&aproxy_usb->usb_lock);

    return;
}
void proxy_usb_close_in_proxy(void *proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    pthread_mutex_lock(&aproxy_usb->usb_lock);

    usb_close_in_proxy(aproxy_usb);

    pthread_mutex_unlock(&aproxy_usb->usb_lock);

    return;
}

void proxy_usb_set_gain(void *proxy_usb, char *path_name)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    char gain_name[MAX_USB_PATH_LEN];

    if (!aproxy_usb->usb_gaincontrol_needed)
        return ;

    strlcpy(gain_name, path_name, MAX_USB_PATH_LEN);
    strlcat(gain_name, "-gain", MAX_USB_PATH_LEN);
    audio_route_apply_and_update_path(aproxy_usb->usb_ar, gain_name);
    ALOGI("proxy-%s: routed to %s", __func__, gain_name);

    return;
}

void proxy_usb_reset_gain(void *proxy_usb, char *path_name)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    char gain_name[MAX_USB_PATH_LEN];

    if (!aproxy_usb->usb_gaincontrol_needed)
        return ;

    strlcpy(gain_name, path_name, MAX_USB_PATH_LEN);
    strlcat(gain_name, "-gain", MAX_USB_PATH_LEN);
    audio_route_reset_and_update_path(aproxy_usb->usb_ar, gain_name);
    ALOGI("proxy-%s: routed to %s", __func__, gain_name);

    return;
}

int proxy_usb_set_parameters(void *proxy_usb, void *parameters)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    struct str_parms *parms = (struct str_parms *)parameters;
    int val;
    int ret = 0;     // for parameter handling
    int status = 0;  // for return value

    ret = str_parms_get_int(parms, AUDIO_PARAMETER_DEVICE_CONNECT, &val);
    if (ret >= 0) {
        if ((audio_devices_t)val == AUDIO_DEVICE_OUT_USB_DEVICE ||
            (audio_devices_t)val == AUDIO_DEVICE_OUT_USB_HEADSET) {
            int card = -1, device = -1;

            ret = str_parms_get_int(parms, AUDIO_PARAMETER_DEVICE_CARD, &val);
            if (ret >= 0)
                card = val;

            ret = str_parms_get_int(parms, AUDIO_PARAMETER_DEVICE_DEVICE, &val);
            if (ret >= 0)
                device = val;

            ALOGI("proxy-%s: connected USB Out Device with card %d / device %d", __func__, card, device);

            if (!aproxy_usb->usb_out_connected && (card != -1 && device != -1)) {
                pthread_mutex_lock(&aproxy_usb->usb_lock);
                aproxy_usb->usb_out_connected = true;
                aproxy_usb->usb_out_pcm_card = card;
                aproxy_usb->usb_out_pcm_device = device;

                usb_config_out_proxy(proxy_usb);

                //check and enable gain-control for connected USB-Device
                usb_audio_gain_control_enable(aproxy_usb);
                pthread_mutex_unlock(&aproxy_usb->usb_lock);
            }
        } else if ((audio_devices_t)val == AUDIO_DEVICE_IN_USB_DEVICE ||
                   (audio_devices_t)val == AUDIO_DEVICE_IN_USB_HEADSET) {
            int card = -1, device = -1;

            ret = str_parms_get_int(parms, AUDIO_PARAMETER_DEVICE_CARD, &val);
            if (ret >= 0)
                card = val;

            ret = str_parms_get_int(parms, AUDIO_PARAMETER_DEVICE_DEVICE, &val);
            if (ret >= 0)
                device = val;

            ALOGI("proxy-%s: connected USB In Device with card %d / device %d", __func__, card, device);

            if (!aproxy_usb->usb_in_connected && (card != -1 && device != -1)) {
                pthread_mutex_lock(&aproxy_usb->usb_lock);
                aproxy_usb->usb_in_connected = true;
                aproxy_usb->usb_in_pcm_card = card;
                aproxy_usb->usb_in_pcm_device = device;

                usb_config_in_proxy(proxy_usb);

                //check and enable gain-control for connected USB-Device
                usb_audio_gain_control_enable(aproxy_usb);
                pthread_mutex_unlock(&aproxy_usb->usb_lock);
            }
        }

        // Check and update usb device clock source information
        if (aproxy_usb->usb_out_connected ||
            aproxy_usb->usb_in_connected) {
            update_usb_clksource_info(true);
        }
    }

    ret = str_parms_get_int(parms, AUDIO_PARAMETER_DEVICE_DISCONNECT, &val);
    if (ret >= 0) {
        if ((audio_devices_t)val == AUDIO_DEVICE_OUT_USB_DEVICE ||
            (audio_devices_t)val == AUDIO_DEVICE_OUT_USB_HEADSET) {
            ALOGI("proxy-%s: disconnected USB Out Device with card %d / device %d", __func__,
                aproxy_usb->usb_out_pcm_card, aproxy_usb->usb_out_pcm_device);

            if (aproxy_usb->usb_out_connected) {
                pthread_mutex_lock(&aproxy_usb->usb_lock);
                usb_close_out_proxy(aproxy_usb);

                aproxy_usb->usb_out_pcm_card = -1;
                aproxy_usb->usb_out_pcm_device = -1;
                aproxy_usb->usb_out_connected = false;

                //check and enable gain-control for connected USB-Device
                usb_audio_gain_control_disable(aproxy_usb);
                pthread_mutex_unlock(&aproxy_usb->usb_lock);
            }
        } else if ((audio_devices_t)val == AUDIO_DEVICE_IN_USB_DEVICE ||
                   (audio_devices_t)val == AUDIO_DEVICE_IN_USB_HEADSET) {
            ALOGI("proxy-%s: disconnected USB In Device with card %d / device %d", __func__,
                aproxy_usb->usb_in_pcm_card, aproxy_usb->usb_in_pcm_device);

            if (aproxy_usb->usb_in_connected) {
                pthread_mutex_lock(&aproxy_usb->usb_lock);
                usb_close_in_proxy(aproxy_usb);

                aproxy_usb->usb_in_pcm_card = -1;
                aproxy_usb->usb_in_pcm_device = -1;
                aproxy_usb->usb_in_connected = false;

                //check and enable gain-control for connected USB-Device
                usb_audio_gain_control_disable(aproxy_usb);
                pthread_mutex_unlock(&aproxy_usb->usb_lock);
            }
        }

        // Check and update usb device clock source information
        if (((audio_devices_t)val == AUDIO_DEVICE_OUT_USB_DEVICE ||
            (audio_devices_t)val == AUDIO_DEVICE_OUT_USB_HEADSET ||
            (audio_devices_t)val == AUDIO_DEVICE_IN_USB_DEVICE ||
            (audio_devices_t)val == AUDIO_DEVICE_IN_USB_HEADSET) &&
            (!aproxy_usb->usb_out_connected && !aproxy_usb->usb_in_connected)) {
            update_usb_clksource_info(false);
        }
    }

    return status;
}

void * proxy_usb_init(void)
{
    struct audio_proxy_usb *aproxy_usb;

    /* Get audio_proxy_usb singleton instance*/
    aproxy_usb = getUSBInstance();
    if (!aproxy_usb) {
        ALOGE("proxy-%s: failed to create for audio_proxy_usb", __func__);
        return NULL;
    }
    // USB PCM Devices
    pthread_mutex_init(&aproxy_usb->usb_lock, (const pthread_mutexattr_t *) NULL);

    pthread_mutex_lock(&aproxy_usb->usb_lock);
    profile_init(&aproxy_usb->usb_out_profile, PCM_OUT);
    aproxy_usb->usb_out_connected = false;
    aproxy_usb->usb_out_status = false;
    aproxy_usb->usb_out_pcm_card = -1;
    aproxy_usb->usb_out_pcm_device = -1;
    aproxy_usb->usb_out_cpcall_prepared = false;

    profile_init(&aproxy_usb->usb_in_profile,  PCM_IN);
    aproxy_usb->usb_in_connected = false;
    aproxy_usb->usb_in_pcm_card = -1;
    aproxy_usb->usb_in_pcm_device = -1;

    //Initialize gain-control varibles
    aproxy_usb->usb_gaincontrol_needed = false;
    aproxy_usb->usb_vid = -1;
    aproxy_usb->usb_pid = -1;

    pthread_mutex_unlock(&aproxy_usb->usb_lock);

    ALOGI("proxy-%s: opened & initialized USB Audio Proxy", __func__);

    return (void *)aproxy_usb;
}

void proxy_usb_deinit(void* proxy_usb)
{
    struct audio_proxy_usb *aproxy_usb = (struct audio_proxy_usb *)proxy_usb;
    pthread_mutex_destroy(&aproxy_usb->usb_lock);

    destroyUSBInstance();
    ALOGI("proxy-%s: audio_proxy_usb instance destroyed", __func__);

    return ;
}
