/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUDIO_USB_PROXY_H
#define AUDIO_USB_PROXY_H

#include <system/audio.h>
#include <hardware/hardware.h>
#include <hardware/audio.h>
#include <hardware/audio_alsaops.h>

#include <audio_utils/resampler.h>
#include <audio_route/audio_route.h>

#include "alsa_device_profile.h"
#include "alsa_device_proxy.h"
#include "alsa_logging.h"

struct audio_proxy_usb
{
    pthread_mutex_t usb_lock;

    alsa_device_profile usb_out_profile;
    alsa_device_proxy   usb_out_proxy;
    bool usb_out_connected;
    bool usb_out_status;
    bool usb_out_cpcall_prepared;

    int  usb_out_pcm_card;
    int  usb_out_pcm_device;

    alsa_device_profile usb_in_profile;
    alsa_device_proxy   usb_in_proxy;
    bool usb_in_connected;

    int  usb_in_pcm_card;
    int  usb_in_pcm_device;

    unsigned int usb_in_channels;
    unsigned int usb_in_rate;

    bool usb_gaincontrol_needed;
    int usb_vid;
    int usb_pid;
    struct audio_route *usb_ar;
};

/* Default values for Media PCM Configuration */
#define DEFAULT_USB_CAPTURE_CHANNELS        1                   // Mono
#define DEFAULT_USB_MEDIA_CHANNELS          2                   // Stereo
#define DEFAULT_USB_MEDIA_SAMPLING_RATE     48000               // 48KHz
#define DEFAULT_USB_MEDIA_FORMAT            PCM_FORMAT_S16_LE   // 16bit PCM

#define MAX_USB_PATH_LEN                    256
#define USB_READ_SIZE                       128


#define AUDIO_PARAMETER_DEVICE_CARD   "card"
#define AUDIO_PARAMETER_DEVICE_DEVICE "device"

/* USB Bundle Device VID (Vendor ID): PID (Product ID) definitions */
#define USB_BUNDLE_VID                      0x04e8
#define USB_BUNDLE_WHITE_PID                0xa037
#define USB_BUNDLE_GRAY_HEADPHONE_PID       0xa04b
#define USB_BUNDLE_GRAY_HEADSET_PID         0xa04c


/* USB Device VID (Vendor ID): PID (Product ID) definitions */
#define USB_BUNDLE_WHITE_GAIN_XML_MIXER_PATH    "/vendor/etc/mixer_usb_white.xml"
#define USB_BUNDLE_GRAY_GAIN_XML_MIXER_PATH     "/vendor/etc/mixer_usb_gray.xml"

extern void update_usb_clksource_info(bool flag);

#endif /* AUDIO_USB_PROXY_H */
